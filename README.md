# Entrance

entrypoints for loaded mods

entrance adds special entrypoints for each loaded mod in order to dynamically load mod-dependent code

each entrypoint has the format of `entrance:modid:type`

`modid` is replaced with the id of the mod to run for, if `modid` isnt loaded the entrypoint wont be run

`type` is replaced with the default fabric entrypoint to use

`main` for `ModInitializer`, `client` for `ClientModInitializer`, and `server` for `DedicatedServerModInitializer`

dont put these entrypoints in the same class as your standard entrypoints please

if your mod defines entrypoints and you dont want entrance to be supported, add these to your fabric.mod.json:

```json
{
  "custom": {
    "entrance:existing_entrypoints": [
      "<existing universal entrypoint>",
      "<other existing universal entrypoint>"
    ],
    "entrance:existing_client_entrypoints": [
      "<existing client only entrypoint>",
      "<other existing client only entrypoint>"
    ],
    "entrance:existing_server_entrypoints": [
      "<existing server only entrypoint>",
      "<other existing server only entrypoint>"
    ]
  }
}
```

this should be safe to jij just get it from jitpack