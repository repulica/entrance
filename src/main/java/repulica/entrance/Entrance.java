package repulica.entrance;

import java.util.List;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.entrypoint.EntrypointContainer;
import net.fabricmc.loader.api.metadata.CustomValue;
import net.fabricmc.loader.api.metadata.ModMetadata;

public class Entrance implements ModInitializer {
	public static final String MODID = "entrance";

	@Override
	public void onInitialize() {
		FabricLoader loader = FabricLoader.getInstance();
		for (ModContainer container : loader.getAllMods()) {
			ModMetadata meta = container.getMetadata();
			List<EntrypointContainer<ModInitializer>> entrypoints = loader.getEntrypointContainers(MODID + ":"
							+ meta.getId() + ":main", ModInitializer.class);
			if (meta.containsCustomValue(MODID + ":existing_entrypoints")) {
				CustomValue.CvArray defined = meta.getCustomValue(MODID + ":existing_entrypoints").getAsArray();
				if (!entrypoints.isEmpty()) {
					StringBuilder builder = new StringBuilder();
					builder.append("Mod ")
							.append(container.getMetadata().getId())
							.append(" defines custom entrypoints (");
					for (int i = 0; i < defined.size(); i++) {
						builder.append(defined.get(i).getAsString());
						if (i != defined.size() - 1) builder.append(", ");
					}
					builder.append("), but ");
					if (entrypoints.size() == 1) {
						builder.append("a mod (")
								.append(entrypoints.get(0).getProvider().getMetadata().getId())
								.append(") is using Entrance instead");
					} else {
						builder.append("multiple mods (");
						for (int i = 0; i < entrypoints.size(); i++) {
							builder.append(entrypoints.get(i).getProvider().getMetadata().getId());
							if (i != entrypoints.size() - 1) builder.append(", ");
						}
						builder.append(") are using Entrance instead");
					}
					throw new UnsupportedOperationException(builder.toString());
				}
			} else {
				entrypoints.forEach(entry -> entry.getEntrypoint().onInitialize());
			}
		}
	}
}
