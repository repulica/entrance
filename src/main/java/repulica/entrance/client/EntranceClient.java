package repulica.entrance.client;

import java.util.List;

import repulica.entrance.Entrance;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.entrypoint.EntrypointContainer;
import net.fabricmc.loader.api.metadata.CustomValue;
import net.fabricmc.loader.api.metadata.ModMetadata;

@Environment(EnvType.CLIENT)
public class EntranceClient implements ClientModInitializer {

	@Override
	public void onInitializeClient() {
		FabricLoader loader = FabricLoader.getInstance();
		for (ModContainer container : loader.getAllMods()) {
			ModMetadata meta = container.getMetadata();
			List<EntrypointContainer<ClientModInitializer>> entrypoints =
					loader.getEntrypointContainers(Entrance.MODID + ":" + meta.getId() + ":client",
							ClientModInitializer.class);
			if (meta.containsCustomValue(Entrance.MODID + ":existing_client_entrypoints")) {
				CustomValue.CvArray defined = meta.getCustomValue(Entrance.MODID + ":existing_client_entrypoints")
						.getAsArray();
				if (!entrypoints.isEmpty()) {
					StringBuilder builder = new StringBuilder();
					builder.append("Mod ")
							.append(container.getMetadata().getId())
							.append(" defines custom client entrypoints (");
					for (int i = 0; i < defined.size(); i++) {
						builder.append(defined.get(i).getAsString());
						if (i != defined.size() - 1) builder.append(", ");
					}
					builder.append("), but ");
					if (entrypoints.size() == 1) {
						builder.append("a mod (")
								.append(entrypoints.get(0).getProvider().getMetadata().getId())
								.append(") is using Entrance instead");
					} else {
						builder.append("multiple mods (");
						for (int i = 0; i < entrypoints.size(); i++) {
							builder.append(entrypoints.get(i).getProvider().getMetadata().getId());
							if (i != entrypoints.size() - 1) builder.append(", ");
						}
						builder.append(") are using Entrance instead");
					}
					throw new UnsupportedOperationException(builder.toString());
				}
			} else {
				entrypoints.forEach(entry -> entry.getEntrypoint().onInitializeClient());
			}
		}
	}
}
